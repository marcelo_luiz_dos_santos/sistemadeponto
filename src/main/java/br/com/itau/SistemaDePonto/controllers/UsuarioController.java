package br.com.itau.SistemaDePonto.controllers;

import br.com.itau.SistemaDePonto.models.Usuario;
import br.com.itau.SistemaDePonto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario cadastrarUsuario(@RequestBody @Valid Usuario usuario){
        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);
        return usuarioObjeto;
    }

    @PutMapping("/{id}")
    public Usuario atualizarUsuario(@PathVariable(name = "id") int id, @RequestBody @Valid Usuario usuario){
        Usuario usuarioObjeto = usuarioService.atualizarUsuario(id, usuario);
        return usuarioObjeto;
    }

    @GetMapping("/{id}")
    public Usuario bucarPorId(@PathVariable(name = "id") int id){
        try{
            Usuario usuario = usuarioService.buscarPorId(id);
            return usuario;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public Iterable<Usuario> exibirTodos(){
        Iterable<Usuario> usuarios = usuarioService.buscarTodosOsUsuarios();
        return usuarios;
    }
}
