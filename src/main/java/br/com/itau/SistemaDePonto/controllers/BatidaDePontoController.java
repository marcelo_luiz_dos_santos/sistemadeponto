package br.com.itau.SistemaDePonto.controllers;

import br.com.itau.SistemaDePonto.models.BatidaDePonto;
import br.com.itau.SistemaDePonto.models.DTOs.BatidaDePontosDTO;
import br.com.itau.SistemaDePonto.services.BatidaDePontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/batidaDePontos")
public class BatidaDePontoController {

    @Autowired
    BatidaDePontoService batidaDePontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BatidaDePonto cadastrarBatidaDePonto(@RequestBody @Valid BatidaDePonto batidaDePonto){
        BatidaDePonto batidaDePontoObjeto = batidaDePontoService.salvarBatidaDePonto(batidaDePonto);
        return batidaDePontoObjeto;
    }

@GetMapping("/{idUsuario}")
public BatidaDePontosDTO bucarPorIdUsuario(@PathVariable(name = "idUsuario") int idUsuario){
    try{
        BatidaDePontosDTO batidaDePontos = batidaDePontoService.buscarBatidaPorUsuario(idUsuario);
        return batidaDePontos;
    }catch (RuntimeException exception){
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
    }
}
}
