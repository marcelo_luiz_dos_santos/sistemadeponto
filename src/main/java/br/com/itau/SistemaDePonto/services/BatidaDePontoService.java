package br.com.itau.SistemaDePonto.services;

import br.com.itau.SistemaDePonto.enums.tipoBatidaEnum;
import br.com.itau.SistemaDePonto.models.BatidaDePonto;
import br.com.itau.SistemaDePonto.models.DTOs.BatidaDePontosDTO;
import br.com.itau.SistemaDePonto.repositories.BatidaDePontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class BatidaDePontoService {

    @Autowired
    private BatidaDePontoRepository batidaDePontoRepository;

    public BatidaDePonto salvarBatidaDePonto(BatidaDePonto batidaDePonto){
        LocalDateTime hora = LocalDateTime.now();
        batidaDePonto.setHoraBatida(hora);
        BatidaDePonto batidaDePontoObjeto = batidaDePontoRepository.save(batidaDePonto);
        return batidaDePontoObjeto;
    }

    public BatidaDePontosDTO buscarBatidaPorUsuario(int idUsuario){
        LocalDateTime batidaEntrada = null;
        LocalDateTime batidaSaida = null;
        long horasTrabalhadas = 0;
        BatidaDePontosDTO batidaDePontosDTO = new BatidaDePontosDTO();

        Iterable<BatidaDePonto> batidaDePontos = batidaDePontoRepository.findAllByIdUsuario(idUsuario);

        for (BatidaDePonto batida : batidaDePontos) {
            if (batida.getTipoBatida() == tipoBatidaEnum.ENTRADA) {
                 batidaEntrada = batida.getHoraBatida();
            } else {
                 batidaSaida = batida.getHoraBatida();
            }
        }
        Duration duracao = Duration.between(batidaEntrada, batidaSaida);
        horasTrabalhadas = duracao.toMinutes();

        batidaDePontosDTO.setHoraBatidaEntrada(batidaEntrada);
        batidaDePontosDTO.setHoraBatidaSaida(batidaSaida);
        batidaDePontosDTO.setHorasTrabalhadas(new BigDecimal((double) horasTrabalhadas / 60).setScale(2, RoundingMode.HALF_UP));

        return batidaDePontosDTO;
    }
}


