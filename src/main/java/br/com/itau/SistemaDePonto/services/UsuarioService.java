package br.com.itau.SistemaDePonto.services;

import br.com.itau.SistemaDePonto.models.Usuario;
import br.com.itau.SistemaDePonto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario){
        LocalDate data = LocalDate.now();
        usuario.setDataDeCadastro(data);
        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }

    public Usuario atualizarUsuario(int id, Usuario usuario){
        if (usuarioRepository.existsById(id)){
            Optional<Usuario> usuarioAtual = usuarioRepository.findById(id);
            usuario.setDataDeCadastro(usuarioAtual.get().getDataDeCadastro());

            usuario.setId(id);
            Usuario usuarioObjeto = usuarioRepository.save(usuario);
            return usuarioObjeto;
        }
        throw new RuntimeException("O usuário não foi encontrado");
    }

    public Usuario buscarPorId(int id) {
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        if (optionalUsuario.isPresent()){
            return optionalUsuario.get();
        }
        throw new RuntimeException("O usuário não foi encontrado");
    }

    public Iterable<Usuario> buscarTodosOsUsuarios(){
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios;
    }
}
