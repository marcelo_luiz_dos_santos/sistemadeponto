package br.com.itau.SistemaDePonto.repositories;

import br.com.itau.SistemaDePonto.models.BatidaDePonto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BatidaDePontoRepository extends CrudRepository<BatidaDePonto, Integer> {
    Iterable<BatidaDePonto> findAllByIdUsuario(int idUsuario);

}
