package br.com.itau.SistemaDePonto.models;

import br.com.itau.SistemaDePonto.enums.tipoBatidaEnum;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class BatidaDePonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private int idUsuario;

    private LocalDateTime horaBatida;

    @NotNull
    private tipoBatidaEnum tipoBatida;

    public BatidaDePonto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public LocalDateTime getHoraBatida() {
        return horaBatida;
    }

    public void setHoraBatida(LocalDateTime horaBatida) {
        this.horaBatida = horaBatida;
    }

    public tipoBatidaEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(tipoBatidaEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
