package br.com.itau.SistemaDePonto.models.DTOs;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class BatidaDePontosDTO {

    private LocalDateTime horaBatidaEntrada;

    private LocalDateTime horaBatidaSaida;

    private BigDecimal horasTrabalhadas;

    public BatidaDePontosDTO() {
    }

    public LocalDateTime getHoraBatidaEntrada() {
        return horaBatidaEntrada;
    }

    public void setHoraBatidaEntrada(LocalDateTime horaBatidaEntrada) {
        this.horaBatidaEntrada = horaBatidaEntrada;
    }

    public LocalDateTime getHoraBatidaSaida() {
        return horaBatidaSaida;
    }

    public void setHoraBatidaSaida(LocalDateTime horaBatidaSaida) {
        this.horaBatidaSaida = horaBatidaSaida;
    }

    public BigDecimal getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public void setHorasTrabalhadas(BigDecimal horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }
}
