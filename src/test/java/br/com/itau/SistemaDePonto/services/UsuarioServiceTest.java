package br.com.itau.SistemaDePonto.services;

import br.com.itau.SistemaDePonto.models.Usuario;
import br.com.itau.SistemaDePonto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    private Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNomeCompleto("Marcelo Luiz");
        usuario.setDataDeCadastro(LocalDate.now());
        usuario.setCpf("05800778027");
        usuario.setEmail("mls@gmail.com");
    }

    @Test
    public void testarBuscarPorId(){
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(usuario.getId())).thenReturn(usuarioOptional);

        Usuario usuarioResposta = usuarioService.buscarPorId(usuario.getId());

        Assertions.assertEquals(usuarioOptional.get(), usuarioResposta);
    }

    @Test
    public void testarBuscarPorTodosOsUsuario() {
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        Iterable<Usuario> usuariosResposta = usuarioService.buscarTodosOsUsuarios();

        Assertions.assertEquals(usuarios, usuariosResposta);
    }

    @Test
    public void testarSalvarUsuario() {
        Usuario usuarioTeste = new Usuario();
        usuarioTeste.setNomeCompleto("Marcelo Luiz 2");
        usuarioTeste.setDataDeCadastro(LocalDate.now());
        usuarioTeste.setCpf("05800778027");
        usuarioTeste.setEmail("mls2@gmail.com");

        Mockito.when(usuarioRepository.save(usuarioTeste)).then(leadLamb -> {
            usuarioTeste.setId(1);
            return usuarioTeste;
        });

        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuarioTeste);

        Assertions.assertEquals(LocalDate.now(), usuarioObjeto.getDataDeCadastro());
        Assertions.assertEquals(1, usuarioTeste.getId());
        Assertions.assertEquals(usuarioObjeto, usuarioTeste);
    }

    @Test
    public void testarAtualizarUsuario(){
        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(usuario.getId())).thenReturn(usuarioOptional);
        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);

        Usuario usuarioObjeto = usuarioService.atualizarUsuario(1, usuario);

        Assertions.assertEquals(usuario, usuarioObjeto);
    }
}
