package br.com.itau.SistemaDePonto.services;

import br.com.itau.SistemaDePonto.enums.tipoBatidaEnum;
import br.com.itau.SistemaDePonto.models.BatidaDePonto;
import br.com.itau.SistemaDePonto.repositories.BatidaDePontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;

@SpringBootTest
public class BatidaDePontoServiceTest {

    @MockBean
    private BatidaDePontoRepository batidaDePontoRepository;

    @Autowired
    private BatidaDePontoService batidaDePontoService;

    private BatidaDePonto batidaDePonto;

    @BeforeEach
    public void setUp() {
        BatidaDePonto batidaDePonto = new BatidaDePonto();
        batidaDePonto.setId(1);
        batidaDePonto.setIdUsuario(1);
        batidaDePonto.setHoraBatida(LocalDateTime.now());
        batidaDePonto.setTipoBatida(tipoBatidaEnum.ENTRADA);

        BatidaDePonto batidaDePonto2 = new BatidaDePonto();
        batidaDePonto2.setId(2);
        batidaDePonto2.setIdUsuario(1);
        batidaDePonto2.setHoraBatida(LocalDateTime.now());
        batidaDePonto2.setTipoBatida(tipoBatidaEnum.SAIDA);
    }

    @Test
    public void testarSalvarBatidaDePonto() {
        BatidaDePonto batidaDePontoTeste = new BatidaDePonto();
        batidaDePontoTeste.setIdUsuario(1);
        batidaDePontoTeste.setHoraBatida(LocalDateTime.now());
        batidaDePontoTeste.setTipoBatida(tipoBatidaEnum.ENTRADA);

        Mockito.when(batidaDePontoRepository.save(batidaDePontoTeste)).then(leadLamb -> {
            batidaDePontoTeste.setId(1);
            return batidaDePontoTeste;
        });
        BatidaDePonto batidaDePontoObjeto = batidaDePontoService.salvarBatidaDePonto(batidaDePontoTeste);

        Assertions.assertEquals(batidaDePontoObjeto, batidaDePontoTeste);
    }
}
