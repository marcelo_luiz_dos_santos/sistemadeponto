package br.com.itau.SistemaDePonto.controllers;

import br.com.itau.SistemaDePonto.enums.tipoBatidaEnum;
import br.com.itau.SistemaDePonto.models.BatidaDePonto;
import br.com.itau.SistemaDePonto.models.Usuario;
import br.com.itau.SistemaDePonto.services.BatidaDePontoService;
import br.com.itau.SistemaDePonto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalDateTime;

@WebMvcTest(BatidaDePontoController.class)
public class BatidaDePontoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BatidaDePontoService batidaDePontoService;

    BatidaDePonto batidaDePonto;

    @Test
    public void testarCadastrarBatidaComSucesso() throws Exception {
        BatidaDePonto batidaDePonto = new BatidaDePonto();
        batidaDePonto.setId(1);
        batidaDePonto.setIdUsuario(1);
        batidaDePonto.setTipoBatida(tipoBatidaEnum.ENTRADA);

        Mockito.when(batidaDePontoService.salvarBatidaDePonto(Mockito.any(BatidaDePonto.class))).then(batidaDePontoObjeto -> {
            batidaDePonto.setHoraBatida(LocalDateTime.now());
            return batidaDePonto;
        });


        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(batidaDePonto);

        mockMvc.perform(MockMvcRequestBuilders.post("/batidaDePontos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

}
