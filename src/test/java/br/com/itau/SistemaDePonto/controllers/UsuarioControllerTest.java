package br.com.itau.SistemaDePonto.controllers;


import br.com.itau.SistemaDePonto.models.Usuario;
import br.com.itau.SistemaDePonto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Arrays;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNomeCompleto("Marcelo Luiz");
        usuario.setDataDeCadastro(LocalDate.now());
        usuario.setCpf("05800778027");
        usuario.setEmail("mls@gmail.com");
    }

    @Test
    public void testarConsultarUsuarioPorIdEncontrado() throws Exception {
        Mockito.when(usuarioService.buscarPorId(Mockito.anyInt())).thenReturn(usuario);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarConsultarUsuarioPorIdNaoEncontrado() throws Exception {
        Mockito.when(usuarioService.buscarPorId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarConsultarTodos() throws Exception {
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioService.buscarTodosOsUsuarios()).thenReturn(usuarios);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarCadastrarUsuarioComSucesso() throws Exception {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNomeCompleto("Marcelo Luiz");
        usuario.setCpf("05800778027");
        usuario.setEmail("mls@gmail.com");

        Mockito.when(usuarioService.salvarUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto -> {
                    usuario.setDataDeCadastro(LocalDate.now());
            return usuario;
        });


        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

}
