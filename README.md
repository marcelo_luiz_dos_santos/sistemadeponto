Projeto: Sistema de Ponto

END POINT para cadastrar usuário: 
Método POST -> "localhost:8080/usuarios"
Body:
    {
        "nomeCompleto": "Marcelo Luiz dos Santos",
        "cpf": "05800778027",
        "email": "MLSA@gmail.com"
    }

END POINT para atualizar usuário: 
Método PUT -> "localhost:8080/usuarios/{id}"
Body:
    {
        "nomeCompleto": "Marcelo Luiz dos Santos",
        "cpf": "05800778027",
        "email": "MLSA@gmail.com"
    }

END POINT para buscar todos os usuários: 
Método GET -> "localhost:8080/usuarios"

END POINT para buscar usuários por ID: 
Método GET -> "localhost:8080/usuarios/{id}"

END POINT para cadastrar batida de ponto do dia do usuário: 
Método POST -> "localhost:8080//batidaDePontos"
Body:
    {
        "idUsuario": 1,
        "tipoBatida": "ENTRADA"
    }
ou
Body:
    {
        "idUsuario": 1,
        "tipoBatida": "SAIDA"
    }


END POINT para buscar batida de ponto do dia do usuário e horas trabalhadas - Funcionalidade apenas para um dia: 
Método POST -> "localhost:8080//batidaDePontos/{idUsuario}"



